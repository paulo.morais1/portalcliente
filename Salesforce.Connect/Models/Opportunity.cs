﻿using Flurl;
using Flurl.Http;
using Salesforce.Connect.Models.Request;
using System.Threading.Tasks;

namespace Salesforce.Connect.Models
{
    public class Opportunity
    {
        public const string table_name = "Opportunity";

        #region fields
        public const string ID = "Id";
        public const string NAME = "name";
        public const string Premiacao_Comissao_Original__c = "Premiacao_Comissao_Original__c";
        public const string StageName = "StageName";
        public const string Amount = "Amount";
        public const string Probability = "Probability";
        public const string ExpectedRevenue = "ExpectedRevenue";
        public const string CloseDate = "CloseDate";
        public const string CPFCNPJConta__c = "CPFCNPJConta__c";
        public const string FID__c = "FID__c";
        public const string IDOportunidade__c = "IDOportunidade__c";
        public const string Imobiliaria__c = "Imobiliaria__c";
        public const string NumeroVenda__c = "NumeroVenda__c";
        public const string Produto__c = "Produto__c";
        public const string Regional__c = "Regional__c";
        public const string StatusIntegracao__c = "StatusIntegracao__c";
        public const string Status_SAFI__c = "Status_SAFI__c";
        public const string TipoDeVenda__c = "TipoDeVenda__c";
        public const string Valor_do_Contrato__c = "Valor_do_Contrato__c";
        public const string Valor_Real_de_Venda__c = "Valor_Real_de_Venda__c";
        public const string Doc_Cliente__c = "Doc_Cliente__c";
        public const string Ano_da_Venda__c = "Ano_da_Venda__c";
        public const string Tipo_de_unidade_do_Produto_vinculado__c = "Tipo_de_unidade_do_Produto_vinculado__c";
        public const string Tempo_Oportunidade__c = "Tempo_Oportunidade__c";
        #endregion fields

        public static async Task<dynamic> GetOportunity(string search, AuthorizationToken Token)
        {

            dynamic response;
            try
            {
                response = await Token.InstanceUrl
                   .AppendPathSegment(SimpleRequest.parameterizedSearch)
                   .WithOAuthBearerToken(Token.AccessToken)
                   .PostJsonAsync(new
                   {
                       q = search,
                       fields = new[] {
                           ID,
                           NAME,
                           Premiacao_Comissao_Original__c
                       },
                       sobjects = new[] {
                           new {
                               fields = new[] {
                                       ID,
                                       NAME,
                                       Premiacao_Comissao_Original__c
                               },
                               name = table_name
                           }
                       }
                   }).ReceiveJson();
            }
            catch (FlurlHttpException ex)
            {
                response = await ex.GetResponseJsonAsync();
            }
            return response;
        }

    }
}
