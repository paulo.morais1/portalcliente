﻿using Newtonsoft.Json.Linq;
using RestSharp;
using Salesforce.Connect.Models.Request;
using System.Threading.Tasks;

namespace Salesforce.Connect.Models
{
    public class EmpreendimentoImages
    {
        public const string table_name = "IntegracaoGoogledrive__c";

        #region fields
        public const string ID = "Id";
        public const string URL__c = "URL__c";
        #endregion

        public static async Task<dynamic> GetEmpreendimentoImages(string IdAnexoPai__c, AuthorizationToken Token)
        {
            string query = "SELECT+" +
                            ID + "+,+" +
                            URL__c +
                            "+FROM+" + table_name +
                            "+where+IdAnexoPai__c='" + IdAnexoPai__c +
                            "' AND TipoDocumento__c = 'Fotos Comunidades'" +
                            "+order+by+CreatedDate+desc";

            string path = (Token.InstanceUrl + SimpleRequest.querySearch + query);

            var client = new RestClient(path);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Token.AccessToken);
            request.AddHeader("Cookie", "BrowserId=mbzmwXrCEeuglF-0nv18tA");
            IRestResponse response = client.Execute(request);

            JObject json = JObject.Parse(response.Content);

            dynamic results = json["records"];

            return results.ToString();
        }
    }
}
