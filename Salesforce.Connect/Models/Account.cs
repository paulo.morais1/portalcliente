﻿using Flurl;
using Flurl.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using Salesforce.Connect.Models.Request;
using System.Threading.Tasks;


namespace Salesforce.Connect.Models
{
    public class Account
    {

        public const string table_name = "Account";

        #region fields
        public const string ID = "Id";
        public const string CPF__c = "CPF__c";
        public const string CodPessoaUAU__c = "CodPessoaUAU__c";
        public const string NAME = "name";
        public const string Email_Adicional__c = "Email_Adicional__c";
        #endregion fields

        public static async Task<ContaDTO> GetAccountByCPF(string CPF, AuthorizationToken Token)
        {

            dynamic response;
            try
            {
                response = await Token.InstanceUrl
                   .AppendPathSegment(SimpleRequest.parameterizedSearch)
                   .WithOAuthBearerToken(Token.AccessToken)
                   .PostJsonAsync(new
                   {
                       q = CPF,
                       sobjects = new[] {
                           new {
                                   fields = new[] {
                                       ID,
                                       CPF__c,
                                       CodPessoaUAU__c,
                                       NAME,
                                       Email_Adicional__c
                                   },
                                   name = table_name,
                                   limit = "1"
                           }
                       }
                   }).ReceiveJson<JObject>();
                dynamic results = response["searchRecords"][0];
                ContaDTO conta = JsonConvert.DeserializeObject<ContaDTO>(results.ToString());
                return conta;
            }
            catch (FlurlHttpException ex)
            {
                response = await ex.GetResponseJsonAsync();
            }
            return response;
        }

        public static async Task<ContaDTO> GetAccount(string id, AuthorizationToken Token)
        {
            string query = "SELECT+" +
                            ID + "+,+" +
                            CPF__c + "+,+" +
                            CodPessoaUAU__c + "+,+" +
                            NAME + "+,+" +
                            Email_Adicional__c +
                            "+FROM+" + table_name + "+where+id='" + id + "'+" +
                            "limit+1";

            string path = (Token.InstanceUrl + SimpleRequest.querySearch + query);

            var client = new RestClient(path);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Token.AccessToken);
            request.AddHeader("Cookie", "BrowserId=mbzmwXrCEeuglF-0nv18tA");
            IRestResponse response = client.Execute(request);

            JObject json = JObject.Parse(response.Content);

            dynamic results = json["records"][0];
            ContaDTO conta = JsonConvert.DeserializeObject<ContaDTO>(results.ToString());

            return conta;
        }
    }
}
