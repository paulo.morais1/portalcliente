﻿using Flurl;
using Flurl.Http;
using Salesforce.Connect.Models.Request;
using System.Threading.Tasks;


namespace Salesforce.Connect.Models
{
    public class Product
    {
        public const string table_name = "Produto__c";

        #region fields
        public const string ID = "id";
        public const string NAME = "name";
        public const string Descricao__c = "Descricao__c";
        public const string StatusUnidade__c = "StatusUnidade__c";
        public const string TipoUnidade__c = "TipoUnidade__c";
        public const string C_digo_Produto_UAU__c = "C_digo_Produto_UAU__c";
        public const string codigosUAU__c = "codigosUAU__c";
        public const string Empreendimento__c = "Empreendimento__c";
        public const string NomeEmpreendimento__c = "NomeEmpreendimento__c";
        public const string PrecoMetroQuadrado__c = "PrecoMetroQuadrado__c";
        public const string AreaPrivativaTotal__c = "AreaPrivativaTotal__c";
        public const string AreaRealTotal__c = "AreaRealTotal__c";
        public const string AreaCobertaFinal__c = "AreaCobertaFinal__c";
        public const string NumeroQuartos__c = "NumeroQuartos__c";
        public const string NumeroBanhos__c = "NumeroBanhos__c";
        public const string NumeroSuites__c = "NumeroSuites__c";
        public const string QuantidadeVagas__c = "QuantidadeVagas__c";
        public const string SalaJantar__c = "SalaJantar__c";
        public const string Andar__c = "Andar__c";
        public const string Regional__c = "Regional__c";
        public const string Leilao__c = "Leilao__c";
        public const string CreatedDate = "CreatedDate";
        public const string CreatedById = "CreatedById";
        public const string LastModifiedDate = "LastModifiedDate";
        public const string LastModifiedById = "LastModifiedById";

        #endregion
        public static async Task<dynamic> GetProduct(string search, AuthorizationToken Token)
        {

            dynamic response;
            try
            {
                response = await Token.InstanceUrl
                   .AppendPathSegment(SimpleRequest.parameterizedSearch)
                   .WithOAuthBearerToken(Token.AccessToken)
                   .PostJsonAsync(new
                   {
                       q = search,
                       fields = new[] {
                           ID,
                           NAME,
                           codigosUAU__c,
                           C_digo_Produto_UAU__c,
                           Empreendimento__c,
                           NomeEmpreendimento__c
                       },
                       sobjects = new[] {
                           new {
                                fields = new[]
                                {
                                   ID,
                                   NAME,
                                   codigosUAU__c,
                                   C_digo_Produto_UAU__c,
                                   Empreendimento__c,
                                   NomeEmpreendimento__c
                               },
                               name = table_name
                           }
                       }
                   }).ReceiveJson();
            }
            catch (FlurlHttpException ex)
            {
                response = await ex.GetResponseJsonAsync();
            }
            return response;
        }
    }
}
