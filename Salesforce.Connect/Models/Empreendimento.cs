﻿using Flurl;
using Flurl.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using Salesforce.Connect.Models.Request;
using System.Threading.Tasks;

namespace Salesforce.Connect.Models
{
    public class Empreendimento
    {
        public const string table_name = "Empreendimento__c";

        #region fields
        public const string ID = "id";
        public const string NAME = "name";
        public const string CNPJ_SPE__c = "CNPJ_SPE__c";
        public const string Tipo_de_Obra__c = "Tipo_de_Obra__c";
        public const string Acabamento__c = "Acabamento__c";
        public const string Alvenaria__c = "Alvenaria__c";
        public const string Estrutura__c = "Estrutura__c";
        public const string areaExterna__c = "areaExterna__c";
        public const string Fundacao__c = "Fundacao__c";
        public const string Instalacao__c = "Instalacao__c";
        public const string TotalAndamentoObra__c = "TotalAndamentoObra__c";
        public const string CondicaoEmpreendimento__c = "CondicaoEmpreendimento__c";
        public const string EstagioEmpreendimento__c = "EstagioEmpreendimento__c";
        public const string DataEntregaPrevisao__c = "DataEntregaPrevisao__c";
        public const string DataEntrega__c = "DataEntrega__c";
        public const string CreatedDate = "CreatedDate";
        public const string LastModifiedDate = "LastModifiedDate";

        #endregion fields

        public static async Task<EmpreendimentoDTO> PostEmpreendimento(string search, AuthorizationToken Token)
        {

            dynamic response;
            try
            {
                response = await Token.InstanceUrl
                   .AppendPathSegment(SimpleRequest.parameterizedSearch)
                   .WithOAuthBearerToken(Token.AccessToken)
                   .PostJsonAsync(new
                   {
                       q = search,
                       sobjects = new[] {
                           new {
                                fields = new[]
                                {
                                    ID,
                                    NAME,
                                    CNPJ_SPE__c,
                                    Tipo_de_Obra__c,
                                    Acabamento__c,
                                    Alvenaria__c,
                                    Estrutura__c,
                                    areaExterna__c,
                                    Fundacao__c,
                                    Instalacao__c,
                                    TotalAndamentoObra__c,
                                    CondicaoEmpreendimento__c,
                                    EstagioEmpreendimento__c,
                                    DataEntregaPrevisao__c,
                                    DataEntrega__c,
                                    CreatedDate,
                                    LastModifiedDate
                               },
                               name = table_name
                           }
                       }
                   }).ReceiveJson<JObject>();
                dynamic results = response["searchRecords"][0];
                EmpreendimentoDTO empreendimento = JsonConvert.DeserializeObject<EmpreendimentoDTO>(results.ToString());
                return empreendimento;
            }
            catch (FlurlHttpException ex)
            {
                response = await ex.GetResponseJsonAsync();
            }
            return response;
        }

        public static async Task<EmpreendimentoDTO> GetEmpreendimento(string id, AuthorizationToken Token)
        {
            string query = "SELECT+" +
                                    ID + "+,+" +
                                    NAME + "+,+" +
                                    CNPJ_SPE__c + "+,+" +
                                    Tipo_de_Obra__c + "+,+" +
                                    Acabamento__c + "+,+" +
                                    Alvenaria__c + "+,+" +
                                    Estrutura__c + "+,+" +
                                    areaExterna__c + "+,+" +
                                    Fundacao__c + "+,+" +
                                    Instalacao__c + "+,+" +
                                    TotalAndamentoObra__c + "+,+" +
                                    CondicaoEmpreendimento__c + "+,+" +
                                    EstagioEmpreendimento__c + "+,+" +
                                    DataEntregaPrevisao__c + "+,+" +
                                    DataEntrega__c + "+,+" +
                                    CreatedDate + "+,+" +
                                    LastModifiedDate +
                                    "+FROM+" + table_name + "+where+id='" + id + "'+" +
                                    "limit+1";

            string path = (Token.InstanceUrl + SimpleRequest.querySearch + query);

            var client = new RestClient(path);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Token.AccessToken);
            request.AddHeader("Cookie", "BrowserId=mbzmwXrCEeuglF-0nv18tA");
            IRestResponse response = client.Execute(request);

            JObject json = JObject.Parse(response.Content);

            dynamic results = json["records"][0];
            EmpreendimentoDTO empreendimento = JsonConvert.DeserializeObject<EmpreendimentoDTO>(results.ToString());

            return empreendimento;
        }
    }
}
