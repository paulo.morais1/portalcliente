﻿using Flurl;
using Flurl.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using Salesforce.Connect.Models.Request;
using System.Threading.Tasks;

namespace Salesforce.Connect.Models
{
    public class Asset
    {

        public const string table_name = "Asset";

        #region fields
        public const string ID = "Id";
        public const string AccountId = "AccountId";
        public const string Doc_Cliente__c = "Doc_Cliente__c";
        public const string NAME = "name";
        public const string IdEmpreendimento__c = "IdEmpreendimento__c";
        public const string CodigosUAU__c = "CodigosUAU__c";
        #endregion

        public static async Task<ImovelDTO> PostAsset(string search, AuthorizationToken Token)
        {

            dynamic response;
            try
            {
                response = await Token.InstanceUrl
                   .AppendPathSegment(SimpleRequest.parameterizedSearch)
                   .WithOAuthBearerToken(Token.AccessToken)
                   .PostJsonAsync(new
                   {
                       q = search,
                       sobjects = new[] {
                           new {
                                   fields = new[] {
                                       ID,
                                       AccountId,
                                       Doc_Cliente__c,
                                       NAME,
                                       IdEmpreendimento__c,
                                       CodigosUAU__c
                                   },
                                   name = table_name,
                                   limit = "1"
                           }
                       }
                   }).ReceiveJson<JObject>();
                dynamic results = response["searchRecords"][0];
                ImovelDTO imovel = JsonConvert.DeserializeObject<ImovelDTO>(results.ToString());
                return imovel;
            }
            catch (FlurlHttpException ex)
            {
                response = await ex.GetResponseJsonAsync();
            }
            return response;
        }

        public static async Task<ImovelDTO> GetAsset(string id, AuthorizationToken Token)
        {
            string query = "SELECT+" +
                            ID + "+,+" +
                            AccountId + "+,+" +
                            Doc_Cliente__c + "+,+" +
                            NAME + "+,+" +
                            IdEmpreendimento__c + "+,+" +
                            CodigosUAU__c +
                            "+FROM+" + table_name + "+where+id='" + id + "'+" +
                            "limit+1";

            string path = (Token.InstanceUrl + SimpleRequest.querySearch + query);

            var client = new RestClient(path);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + Token.AccessToken);
            request.AddHeader("Cookie", "BrowserId=mbzmwXrCEeuglF-0nv18tA");
            IRestResponse response = client.Execute(request);

            JObject json = JObject.Parse(response.Content);

            dynamic results = json["records"][0];
            ImovelDTO imovel = JsonConvert.DeserializeObject<ImovelDTO>(results.ToString());

            return imovel;
        }
    }
}
