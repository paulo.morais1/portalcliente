﻿using Newtonsoft.Json;
using RestSharp;

namespace Salesforce.Connect.Models.Request
{
    public class EmpreendimentoDTO
    {
        [JsonProperty("Id")]
        public string id { get; set; }

        [JsonProperty("Name")]
        public string name { get; set; }

        [JsonProperty("CNPJ_SPE__c")]
        public string cnpj { get; set; }

        [JsonProperty("Tipo_de_Obra__c")]
        public string tipo_de_obra { get; set; }

        [JsonProperty("Acabamento__c")]
        public double acabamento { get; set; }

        [JsonProperty("Alvenaria__c")]
        public double alvenaria { get; set; }

        [JsonProperty("Estrutura__c")]
        public string estrutura { get; set; }

        [JsonProperty("areaExterna__c")]
        public string area_externa { get; set; }

        [JsonProperty("Fundacao__c")]
        public string fundacao { get; set; }

        [JsonProperty("Instalacao__c")]
        public string instalacao { get; set; }

        [JsonProperty("TotalAndamentoObra__c")]
        public string total_andamento_obra { get; set; }

        [JsonProperty("CondicaoEmpreendimento__c")]
        public string condicao_empreendimento { get; set; }

        [JsonProperty("EstagioEmpreendimento__c")]
        public string estagio_empreendimento { get; set; }

        [JsonProperty("DataEntregaPrevisao__c")]
        public string data_entrega_previsao { get; set; }

        [JsonProperty("DataEntrega__c")]
        public string data_entrega { get; set; }

        [JsonProperty("CreatedDate")]
        public string created_date { get; set; }

        [JsonProperty("LastModifiedDate")]
        public string last_modified_date { get; set; }
    }
}
