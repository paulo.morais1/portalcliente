﻿using Newtonsoft.Json;

namespace Salesforce.Connect.Models.Request
{
    public class ContaDTO
    {
        [JsonProperty("Id")]
        public string Id { get; set; }

        [JsonProperty("CPF__c")]
        public string CPF { get; set; }

        [JsonProperty("CodPessoaUAU__c")]
        public string CodigoPessoaUAU { get; set; }

        [JsonProperty("name")]
        public string Nome { get; set; }

        [JsonProperty("Email_Adicional__c")]
        public string Email { get; set; }
    }
}
