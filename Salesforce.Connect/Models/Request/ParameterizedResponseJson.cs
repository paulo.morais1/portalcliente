﻿using Newtonsoft.Json;

namespace Salesforce.Connect.Models.Request
{
    class ParameterizedResponseJson
    {
        [JsonProperty("searchRecords")]
        public JsonArrayAttribute searchRecords { get; set; }
    }
}
