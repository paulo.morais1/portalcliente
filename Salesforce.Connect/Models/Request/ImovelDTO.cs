﻿using Newtonsoft.Json;

namespace Salesforce.Connect.Models.Request
{
    public class ImovelDTO
    {
        [JsonProperty("Id")]
        public string Id { get; set; }

        [JsonProperty("AccountId")]
        public string AccountId { get; set; }

        [JsonProperty("Doc_Cliente__c")]
        public string CPF { get; set; }

        [JsonProperty("Name")]
        public string Nome { get; set; }

        [JsonProperty("IdEmpreendimento__c")]
        public string IdEmpreendimento { get; set; }

        [JsonProperty("CodigosUAU__c")]
        public string IdUAU { get; set; }
    }
}
