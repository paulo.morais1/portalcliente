﻿using SforceService;
using System;
using System.Collections.Generic;
using System.Text;

namespace Salesforce.Connect.Models.Request
{
    public class SimpleRequest
    {
        public const String parameterizedSearch = "/services/data/v51.0/parameterizedSearch/";
        public const String querySearch = "/services/data/v50.0/query/?q=";

        public string q { get; set; }
        public List<string> fields { get; set; }
        public List<sObject> sobjects { get; set; }
        public string @in { get; set; }
        public int overallLimit { get; set; }
        public int defaultLimit { get; set; }

    }
}
