﻿using Newtonsoft.Json;

namespace Salesforce.Connect.Models.Request
{
    public class EmpreendimentoImagensDTO
    {
        [JsonProperty("Id")]
        public string ID { get; set; }

        [JsonProperty("URL__c")]
        public string url { get; set; }
    }
}
