﻿using Flurl;
using Flurl.Http;
using Salesforce.Connect.Models;
using System.Threading.Tasks;

namespace Salesforce.Connect
{
    public class SalesforceConnect
    {
        public AuthorizationToken Token { get; set; }
        private string SfUri = "https://test.salesforce.com/services/oauth2/token";
        public SalesforceConnect()
        {

        }

        public async Task<bool> LoginAsync()
        {
            try
            {

                var result = await SfUri
                                    .SetQueryParams(new
                                    {
                                        grant_type = "password",
                                        client_id = "3MVG9ysJNY7CaIHnXtkJBm9UdfMasBtZiZkEb6NwxyUtG.r2suukC7JmwAcTPJBZalAPvJ7aqiIXZ_O1AjtPB",
                                        client_secret = "78F29289BF3FBA1F2DCBEAEE230D7B6F54CED8F7C60B5E622CD95AB0D8EF0292",
                                        username = "integracao@direcional.com.br.prd.hmgfull",
                                        password = "sales@2021A6Foy5SwIMWQju5gjPnY4Gl8",
                                    })
                                    .PostAsync()
                                    .ReceiveJson<AuthorizationToken>();
                Token = result;
                return true;
            }
            catch
            {
                return false;

            }

        }

        public async Task<dynamic> SelectQuery(string value)
        {

            dynamic response;
            try
            {

                response = await Token.InstanceUrl
                   .AppendPathSegment("/services/data/v51.0/parameterizedSearch/q=Contact&sobject=Contact")
                   .WithOAuthBearerToken(Token.AccessToken)
                   .PostAsync();
            }
            catch (FlurlHttpException ex)
            {
                response = await ex.GetResponseJsonAsync();
            }
            return response;
        }
    }
}