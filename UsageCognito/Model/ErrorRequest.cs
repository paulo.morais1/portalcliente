﻿using Newtonsoft.Json;

namespace UsageCognito.Model
{
    public class ErrorRequest
    {
        [JsonProperty("message")]
        public string Message { get; set; }
    }
}
