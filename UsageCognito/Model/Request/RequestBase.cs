﻿using System.Collections.Generic;

namespace UsageCognito.Model.Request
{
    public class RequestBase
    {
        public string ClientId { get; set; }
        public string SecretHash { get; set; }
        public Dictionary<string, string> ClientMetadata { get; set; }
        public AnalyticsMetadata AnalyticsMetadata { get; set; }
        public UserContextData UserContextData { get; set; }
    }
    public class AnalyticsMetadata
    {
        public string AnalyticsEndpointId { get; set; }
    }
    public class UserContextData
    {
        public string EncodedData { get; set; }
    }
}