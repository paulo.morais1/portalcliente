﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UsageCognito.Model.Request
{
    public class SignUpRequest : RequestBase
    {
        public string Password { get; set; }
        public string Username { get; set; }
        public List<UserAttribute> UserAttributes { get; set; }
    }
    public class UserAttribute
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }

    public class ValidationData
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
