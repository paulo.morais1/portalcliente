﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UsageCognito.Model.Request
{
    class InitiateAuthRequest : RequestBase
    {
        public string AuthFlow { get; set; }
        public Dictionary<string, string> AuthParameters { get; set; }
    }
}
