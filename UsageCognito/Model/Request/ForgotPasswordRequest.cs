﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UsageCognito.Model.Request
{
    public class ForgotPasswordRequest
    {
        public string Username { get; set; }
    }
}
