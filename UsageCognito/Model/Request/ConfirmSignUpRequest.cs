﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UsageCognito.Model.Request
{
    public class ConfirmSignUpRequest : RequestBase
    {
        public string ConfirmationCode { get; set; }
        public bool ForceAliasCretion { get; set; }
        public string Username { get; set; }
    }
}
