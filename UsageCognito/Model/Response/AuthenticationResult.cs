﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UsageCognito.Model.Response
{
    class AuthenticationResult
    {
        public string AccessToken { get; set; }
        public int ExpiresIn { get; set; }
        public string IdToken { get; set; }
        [JsonProperty ("NewDeviceMetadata")]
        public NewDeviceMetadata newDeviceMetadata { get; set; }
        public string RefreshToken { get; set; }
        public string TokenType { get; set; }
    }
}
