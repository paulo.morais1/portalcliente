﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UsageCognito.Model.Response
{
    class CodeDeliveryDetails
    {
        public string AttributeName { get; set; }
        public string DeliveryMedium { get; set; }
        public string Destination { get; set; }
    }
}
