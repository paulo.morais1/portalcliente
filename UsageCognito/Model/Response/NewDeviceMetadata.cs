﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UsageCognito.Model.Response
{
    class NewDeviceMetadata
    {
        public string DeviceGroupKey { get; set; }
        public string DeviceKey { get; set; }
    }
}
