﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UsageCognito.Model.Response
{
    class ForgotPasswordResponse
    {
        [JsonProperty("CodeDeliveryDetails")]
        public CodeDeliveryDetails codeDeliveryDetails { get; set; }
    }
}
