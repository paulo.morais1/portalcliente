﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UsageCognito.Model.Response
{
    class InitiateAuthResponse
    {
        [JsonProperty("AuthenticationResult")]
        public AuthenticationResult authenticationResult { get; set; }
        public Dictionary<string, string> ChallengeParameters { get; set; }
        public string ChallengeName { get; set; }
        public string Session { get; set; }

    }
}
