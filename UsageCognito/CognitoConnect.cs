﻿using Amazon;
using Amazon.CognitoIdentityProvider;
using Amazon.Runtime;
using Flurl;
using Flurl.Http;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;
using UsageCognito.Model;
using UsageCognito.Model.Request;
using UsageCognito.Model.Response;

namespace UsageCognito
{
    public class CognitoConnect
    {
        public AmazonCognitoIdentityProviderClient Connect { get; set; }
        public CognitoConnect(IConfiguration configuration)
        {
            Connect = new AmazonCognitoIdentityProviderClient(configuration["Cognito:Login"], configuration["Cognito:Senha"], RegionEndpoint.USEast2);
        }
    }
}