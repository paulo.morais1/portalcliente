﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using System.Net;

namespace UsageCognito
{
    public static class CognitoAuthentication
    {
        public static void AddCognitoAuthentication(this IServiceCollection services, IConfiguration Configuration)
        {
            services.AddAuthentication(o => o.DefaultScheme = JwtBearerDefaults.AuthenticationScheme).AddJwtBearer(options =>
            {
                options.Audience = Configuration["cognito:clientId"];
                options.TokenValidationParameters = TokenValidationParameters(Configuration);
                options.IncludeErrorDetails = true;
                options.SaveToken = true;
                options.Authority = Configuration["cognito:issuer"];
                options.RequireHttpsMetadata = false;
            });
        }

        private static TokenValidationParameters TokenValidationParameters(IConfiguration Configuration)
        {
            return new TokenValidationParameters
            {
                IssuerSigningKeyResolver = (s, securityToken, identifier, parameters) =>
                {
                    var json = new WebClient().DownloadString($"{parameters.ValidIssuer}/.well-known/jwks.json");
                    var keys = new JsonWebKeySet(json).Keys;
                    return keys;
                },

                ValidIssuer = $"https://cognito-idp.{Configuration["cognito:Region"]}.amazonaws.com/{Configuration["cognito:PoolId"]}",
                ValidateIssuerSigningKey = true,
                ValidateIssuer = true,
                ValidateLifetime = true,
                ValidAudience = Configuration["cognito:ClientId"],
                ValidateAudience = false,
            };
        }
    }
}