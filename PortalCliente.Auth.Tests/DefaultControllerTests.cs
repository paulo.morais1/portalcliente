using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using PortalCliente.Auth.Api;
using PortalCliente.Auth.Tests.Models;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using Xunit;

namespace PortalCliente.Auth.Tests
{
    public class DefaultControllerTests
    {
        private readonly HttpClient _client;

        public DefaultControllerTests()
        {
            var server = new TestServer(new WebHostBuilder()
                .UseEnvironment("Development")
                .UseStartup<Startup>());
            _client = server.CreateClient();
        }

        [Fact]
        public async Task GetRoot_ReturnsSuccessAndStatusUp()
        {
            // Arrange
            var request = new HttpRequestMessage(new HttpMethod("GET"), "/api/health");

            // Act
            var response = await _client.SendAsync(request);

            // Assert
            response.EnsureSuccessStatusCode();
            Assert.NotNull(response.Content);
            var responseObject = JsonSerializer.Deserialize<ResponseType>(
                await response.Content.ReadAsStringAsync(),
                new JsonSerializerOptions { PropertyNameCaseInsensitive = true });
            Assert.Equal("Healthy", responseObject?.Status);
        }
    }
}