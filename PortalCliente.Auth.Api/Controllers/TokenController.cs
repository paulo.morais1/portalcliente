﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Linq;
using System.Net;

namespace PortalCliente.Auth.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class TokenController : Controller
    {
        private readonly ILogger<TokenController> _logger;

        public TokenController(ILogger<TokenController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        [Authorize("Bearer")]
        [ProducesResponseType((int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(JsonResult), (int)HttpStatusCode.OK)]
        public ActionResult<string> GetUser()
        {
            _logger.LogInformation("Login aceito");
            return Ok(new JsonResult(from c in User.Claims
                                     select new
                                     {
                                         c.Type,
                                         c.Value
                                     }));
        }
    }
}