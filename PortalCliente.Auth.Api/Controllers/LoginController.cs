﻿using Amazon.CognitoIdentityProvider;
using Amazon.CognitoIdentityProvider.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using PortalCliente.Auth.Api.Model;
using Salesforce.Connect;
using Salesforce.Connect.Models;
using Salesforce.Connect.Models.Request;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using UsageCognito;

namespace PortalCliente.Auth.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class LoginController : ControllerBase
    {
        private readonly ILogger<LoginController> _logger;
        private readonly IConfiguration _configuration;

        public LoginController(ILogger<LoginController> logger, IConfiguration configuration)
        {
            _logger = logger;
            _configuration = configuration;
        }

        [System.Web.Http.HttpPost]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(SignUpResponse), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<SignUpResponse>> Registrar(string CPF)
        {
            _logger.LogInformation($"O CPF:{CPF} iniciou o cadastro");
            ContaDTO contact = default;
            SalesforceConnect salesforceConnect = new SalesforceConnect();
            if (await salesforceConnect.LoginAsync())
            {
                contact = await Account.GetAccountByCPF(CPF, salesforceConnect.Token);
                if (contact is null) return BadRequest("CPF não localizado");

                var newUser = new SignUpRequest
                {
                    ClientId = _configuration["Cognito:ClientId"],
                    Username = contact.Email,
                    Password = new Random().Next().ToString(),
                    UserAttributes = new List<AttributeType> { new AttributeType { Name = "custom:CPF", Value = CPF } }

                };
                var amazonCognito = new CognitoConnect(_configuration).Connect;
                try
                {

                    var signUp = await amazonCognito.SignUpAsync(newUser);
                    return Ok(signUp);
                }
                catch (Exception Ex)
                {
                    return BadRequest(Ex.Message);
                }

            }
            return BadRequest("Falhou na conexão com o Sales");
        }
        [HttpPost]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(ConfirmSignUpResponse), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<ConfirmSignUpResponse>> ConfirmarRegitro(string Username, string ConfirmationCode)
        {


            var request = new ConfirmSignUpRequest
            {
                ClientId = _configuration["Cognito:ClientId"],
                Username = Username,
                ConfirmationCode = ConfirmationCode,
            };
            var amazonCognito = new CognitoConnect(_configuration).Connect;
            try
            {

                var response = await amazonCognito.ConfirmSignUpAsync(request);
                return Ok(response);
            }
            catch (Exception Ex)
            {
                return BadRequest(Ex.Message);
            }

        }
        [HttpPost]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(InitiateAuthResponse), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<InitiateAuthResponse>> Acessar(User User)
        {

            var login = new Dictionary<string, string>(2);
            login.Add("USERNAME", User.Email);
            login.Add("PASSWORD", User.Password);

            var newUser = new InitiateAuthRequest
            {
                ClientId = _configuration["Cognito:ClientId"],
                AuthFlow = AuthFlowType.USER_PASSWORD_AUTH,
                AuthParameters = login
            };
            var amazonCognito = new CognitoConnect(_configuration).Connect;
            try
            {

                var signUp = await amazonCognito.InitiateAuthAsync(newUser);
                return Ok(signUp);
            }
            catch (Exception Ex)
            {
                return BadRequest(Ex.Message);
            }

        }
        [HttpPost]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(ForgotPasswordResponse), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<ForgotPasswordResponse>> RecuperarSenha(string Username)
        {


            var request = new ForgotPasswordRequest
            {
                ClientId = _configuration["Cognito:ClientId"],
                Username = Username
            };
            var amazonCognito = new CognitoConnect(_configuration).Connect;
            try
            {

                var response = await amazonCognito.ForgotPasswordAsync(request);
                return Ok(response);
            }
            catch (Exception Ex)
            {
                return BadRequest(Ex.Message);
            }

        }

        [HttpPost]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(ConfirmForgotPasswordResponse), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<ConfirmForgotPasswordResponse>> ConfirmarSenha(string Username, string ConfirmationCode, string Password)
        {


            var request = new ConfirmForgotPasswordRequest
            {
                ClientId = _configuration["Cognito:ClientId"],
                Username = Username,
                ConfirmationCode = ConfirmationCode,
                Password = Password
            };
            var amazonCognito = new CognitoConnect(_configuration).Connect;
            try
            {

                var response = await amazonCognito.ConfirmForgotPasswordAsync(request);
                return Ok(response);
            }
            catch (Exception Ex)
            {
                return BadRequest(Ex.Message);
            }

        }

    }
}