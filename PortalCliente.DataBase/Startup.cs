using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Npgsql;
using NpgsqlTypes;
using PortalCliente.DataBase.Controllers;
using PortalCliente.DataBase.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PortalCliente.DataBase
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddRazorPages();

            // Add framework services.
            services.AddMvc();
            services.AddEntityFrameworkNpgsql()
                    .AddDbContext<EmpreendimentoContext>(options => options.UseNpgsql(Helpers.GetRDSConnectionString(Configuration)));

            /*
            var contextOptions = new DbContextOptionsBuilder<EmpreendimentoContext>()
            .UseNpgsql(Helpers.GetRDSConnectionString(Configuration))
            .Options;

            using var context = new EmpreendimentoContext(contextOptions);

            Empreendimento empreendimento = new Empreendimento("testeController");
            EmpreendimentoController empreendimentoController = new EmpreendimentoController(context);
            using (NpgsqlConnection conn = new NpgsqlConnection(Helpers.GetRDSConnectionString(Configuration)))
            {
                conn.Open();
                var result = empreendimentoController.CreateEmpreendimento(empreendimento);
                conn.Close();
            }
            */

            /*
            string sql = "INSERT INTO empreendimento (name) VALUES ('testeAvulso');";
            string connection_string = Helpers.GetRDSConnectionString(Configuration);

            using (NpgsqlConnection conn = new NpgsqlConnection(connection_string))
            {
                conn.Open();
                NpgsqlCommand cmd = new NpgsqlCommand(sql, conn);
                cmd.ExecuteNonQuery();
                conn.Close();
            }
            */
            

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapRazorPages();
            });
        }
    }
}
