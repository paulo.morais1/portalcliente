﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace PortalCliente.DataBase.Models
{
    public class EmpreendimentoContext : DbContext
    {
        public DbSet<Empreendimento> Empreendimento { get; set; }

        public EmpreendimentoContext(DbContextOptions<EmpreendimentoContext> options) :
            base(options)
        {
        }
    }
}
