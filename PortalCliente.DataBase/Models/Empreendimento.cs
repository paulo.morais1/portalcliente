﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PortalCliente.DataBase.Models
{
    [Table("Empreendimento")]
    public class Empreendimento
    {
        //public static string table_name = "Empreendimento";

        [Key]
        public int id { get; set; }
        public string name { get; set; }

        public Empreendimento(string name)
        {
            this.name = name;
            //this.id = 8;
        }
    }
}
