﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace PortalCliente.DataBase.Models
{
    public class Helpers
    {

        public static string GetRDSConnectionString(IConfiguration _configuration)
        {

            string dbname = _configuration["Database:dbname"];

            if (string.IsNullOrEmpty(dbname)) return null;

            string username = _configuration["Database:username"];
            string password = _configuration["Database:password"];
            string hostname = _configuration["Database:hostname"];
            string port = _configuration["Database:port"];

            return "Host = " + hostname +
                   ";Port = " + port +
                   ";Database = " + dbname +
                   ";Username = " + username +
                   ";Password = " + password + ";";
        }
    }
}
