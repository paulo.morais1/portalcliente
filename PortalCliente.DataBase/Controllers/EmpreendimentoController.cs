﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PortalCliente.DataBase.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PortalCliente.DataBase.Controllers
{
    [Produces("application/json")]
    [Route("api/empreendimento")]
    public class EmpreendimentoController : Controller
    {
        private readonly EmpreendimentoContext _context;

        public EmpreendimentoController(EmpreendimentoContext empreendimentoContext)
        {
            this._context = empreendimentoContext;
        }

        // GET: api/Empreendimento
        [HttpGet]
        public IEnumerable<Empreendimento> GetEmpreendimento()
        {
            return _context.Empreendimento;
        }

        // GET: api/Empreendimento
        [HttpGet("{id}")]
        public async Task<IActionResult> GetEmpreendimento([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var empreendimento = await _context.Empreendimento.SingleOrDefaultAsync(m => m.id == id);

            if (empreendimento == null)
            {
                return NotFound();
            }

            return Ok(empreendimento);
        }


        // GET: EmpreendimentoController
        public ActionResult Index()
        {
            return View();
        }

        // GET: EmpreendimentoController/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: EmpreendimentoController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: EmpreendimentoController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        //[HttpPost]
        public async Task<IActionResult> CreateEmpreendimento(Empreendimento empreendimento)
        {
            _context.Add(empreendimento);
            await _context.SaveChangesAsync();
            return CreatedAtAction("GetEmpreendimento", new { id = empreendimento.id }, empreendimento);
            //return Ok(empreendimento);
        }

        // GET: EmpreendimentoController/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: EmpreendimentoController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: EmpreendimentoController/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: EmpreendimentoController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
