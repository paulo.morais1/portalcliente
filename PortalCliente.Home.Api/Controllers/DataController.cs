﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Salesforce.Connect;
using Salesforce.Connect.Models;
using System;
using System.Net;
using System.Threading.Tasks;

namespace PortalCliente.Home.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class DataController : Controller
    {
        private readonly ILogger<DataController> _logger;

        public DataController(ILogger<DataController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.OK)]
        public async Task<ActionResult> getImovel(string id)
        {
            try
            {
                SalesforceConnect salesforceConnect = new SalesforceConnect();
                if (await salesforceConnect.LoginAsync())
                {
                    var result = await Asset.GetAsset(id, salesforceConnect.Token);
                    return Ok(result);
                }
                else
                {
                    return BadRequest("Não conseguiu logar na SalesForce!");
                }
            }
            catch (Exception Ex)
            {
                return BadRequest(Ex.Message);
            }
        }

        [HttpGet]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.OK)]
        public async Task<ActionResult> getEmpreendimento(string id)
        {
            try
            {
                SalesforceConnect salesforceConnect = new SalesforceConnect();
                if (await salesforceConnect.LoginAsync())
                {
                    var result = await Empreendimento.GetEmpreendimento(id, salesforceConnect.Token);
                    return Ok(result);
                }
                else
                {
                    return BadRequest("Não conseguiu logar na SalesForce!");
                }
            }
            catch (Exception Ex)
            {
                return BadRequest(Ex.Message);
            }
        }

        [HttpGet]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.OK)]
        public async Task<ActionResult> getImagemEmpreendimento(string id)
        {
            try
            {
                SalesforceConnect salesforceConnect = new SalesforceConnect();
                if (await salesforceConnect.LoginAsync())
                {
                    var result = await EmpreendimentoImages.GetEmpreendimentoImages(id, salesforceConnect.Token);
                    return Ok(result);
                }
                else
                {
                    return BadRequest("Não conseguiu logar na SalesForce!");
                }
            }
            catch (Exception Ex)
            {
                return BadRequest(Ex.Message);
            }
        }


        [HttpGet]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.OK)]
        public async Task<ActionResult> getContaPeloCPF(string cpf)
        {
            try
            {
                SalesforceConnect salesforceConnect = new SalesforceConnect();
                if (await salesforceConnect.LoginAsync())
                {
                    var result = await Account.GetAccountByCPF(cpf, salesforceConnect.Token);
                    return Ok(result);
                }
                else
                {
                    return BadRequest("Não conseguiu logar na SalesForce!");
                }
            }
            catch (Exception Ex)
            {
                return BadRequest(Ex.Message);
            }
        }

        [HttpGet]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.OK)]
        public async Task<ActionResult> getConta(string id)
        {
            try
            {
                SalesforceConnect salesforceConnect = new SalesforceConnect();
                if (await salesforceConnect.LoginAsync())
                {
                    var result = await Account.GetAccount(id, salesforceConnect.Token);
                    return Ok(result);
                }
                else
                {
                    return BadRequest("Não conseguiu logar na SalesForce!");
                }
            }
            catch (Exception Ex)
            {
                return BadRequest(Ex.Message);
            }
        }

    }
}